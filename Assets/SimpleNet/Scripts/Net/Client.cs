﻿using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class Client : Host
{
	bool isConnected;

	int hostId;
	int connectionId;

	int clientServerId;

	public Client(HostTopology topology, int port)
	{
		hostId = NetworkTransport.AddHost(topology, 0);

		byte error;
		connectionId = NetworkTransport.Connect(hostId, "127.0.0.1", port, 0, out error);

		isConnected = true;

		Debug.LogFormat("Client started with socket id: ({0})", hostId);
	}

	public void Update()
	{
		if (!isConnected) return;

		int recHostId;
		int connectionId;
		int channelId;
		byte[] recBuffer = new byte[1024];
		int bufferSize = 1024;
		int dataSize;
		byte error;

		NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
		switch (recData)
		{
			case NetworkEventType.DataEvent:
				ReceiveData(recBuffer);
				break;
		}
	}

	public void ReceiveData(byte[] data)
	{
		MemoryStream stream = new MemoryStream(data);
		BinaryReader reader = new BinaryReader(stream);

		CommandType cmdType = (CommandType)reader.ReadInt32();

		switch (cmdType)
		{
			case CommandType.Message:
				{
					MessageCommand cmd = new MessageCommand().Deserialize(data) as MessageCommand;
					Debug.LogFormat("Message from server: ({0}) client id: ({1}) received: {2}", connectionId, cmd.id, cmd.message);
					break;
				}
			case CommandType.Move:
				{
					MoveCommand cmd = new MoveCommand().Deserialize(data) as MoveCommand;
					Debug.LogFormat("Move from server: ({0}) received: {1}", connectionId, cmd.pos);
					break;
				}
			case CommandType.OnClientConnected:
				{
					OnClientConnectedCommand cmd = new OnClientConnectedCommand().Deserialize(data) as OnClientConnectedCommand;
					clientServerId = cmd.id;
					Debug.LogFormat("On client connected recieved, id from server: ({0})", cmd.id);
					break;
				}
		}

		reader.Close();
		stream.Close();
	}

	public void SendData(byte[] data, int channelId, int connectionId = -1)
	{
		byte error;
		NetworkTransport.Send(hostId, this.connectionId, channelId, data, data.Length, out error);
		Debug.LogFormat("Data sent from client: ({0})", this.connectionId);
	}

	public void SendCommand(Command command, int channelId, int connectionId = -1)
	{
		SendData(command.Serialize(), channelId);
	}

	public int GetId() {
		return clientServerId;
	}
}
