﻿public interface Command
{
	byte[] Serialize();
	Command Deserialize(byte[] data);
}
