﻿using System.IO;

public class MessageCommand : Command
{
	public int id;
	public string message;

	public MessageCommand(int id = -1, string message = "")
	{
		this.id = id;
		this.message = message;
	}

	public byte[] Serialize()
	{
		MemoryStream stream = new MemoryStream();
		BinaryWriter writer = new BinaryWriter(stream);

		writer.Write((int)CommandType.Message);
		writer.Write(id);
		writer.Write(message);

		writer.Close();
		stream.Close();

		return stream.GetBuffer();
	}

	public Command Deserialize(byte[] data)
	{
		MemoryStream stream = new MemoryStream(data);
		BinaryReader reader = new BinaryReader(stream);

		CommandType command = (CommandType)reader.ReadInt32();
		id = reader.ReadInt32();
		message = reader.ReadString();

		reader.Close();
		stream.Close();

		return this;
	}
}
