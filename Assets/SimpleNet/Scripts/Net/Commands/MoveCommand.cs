﻿using System.IO;
using UnityEngine;

public class MoveCommand : Command
{
	public Vector3 pos;

	public MoveCommand(Vector3 pos = new Vector3())
	{
		this.pos = pos;
	}

	public byte[] Serialize()
	{
		MemoryStream stream = new MemoryStream();
		BinaryWriter writer = new BinaryWriter(stream);

		writer.Write((int)CommandType.Move);
		writer.Write(pos.x);
		writer.Write(pos.y);
		writer.Write(pos.z);

		writer.Close();
		stream.Close();

		return stream.GetBuffer();
	}

	public Command Deserialize(byte[] data)
	{
		MemoryStream stream = new MemoryStream(data);
		BinaryReader reader = new BinaryReader(stream);

		CommandType command = (CommandType)reader.ReadInt32();
		float x = reader.ReadSingle();
		float y = reader.ReadSingle();
		float z = reader.ReadSingle();

		pos = new Vector3(x, y, z);

		reader.Close();
		stream.Close();

		return this;
	}
}
