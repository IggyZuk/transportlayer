﻿using System.IO;

public class OnClientConnectedCommand : Command
{
	public int id;

	public OnClientConnectedCommand(int id = -1)
	{
		this.id = id;
	}

	public byte[] Serialize()
	{
		MemoryStream stream = new MemoryStream();
		BinaryWriter writer = new BinaryWriter(stream);

		writer.Write((int)CommandType.OnClientConnected);
		writer.Write(id);

		writer.Close();
		stream.Close();

		return stream.GetBuffer();
	}

	public Command Deserialize(byte[] data)
	{
		MemoryStream stream = new MemoryStream(data);
		BinaryReader reader = new BinaryReader(stream);

		CommandType command = (CommandType)reader.ReadInt32();
		id = reader.ReadInt32();

		reader.Close();
		stream.Close();

		return this;
	}
}
