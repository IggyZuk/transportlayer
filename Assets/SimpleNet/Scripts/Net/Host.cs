public interface Host
{
	void Update();
	void SendData(byte[] data, int channelId, int connectionId);
	void SendCommand(Command command, int channelId, int connectionId = -1);
	int GetId();
}