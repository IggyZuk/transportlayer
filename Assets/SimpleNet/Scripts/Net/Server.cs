﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Linq;

public class Server : Host
{
	bool isStarted;

	int hostId;
	//int webHostId;

	List<int> clientIds = new List<int>();

	public Server(HostTopology topology, int port)
	{
		hostId = NetworkTransport.AddHost(topology, port);
		//hostId = NetworkTransport.AddWebsocketHost(topology, port, null);

		isStarted = true;

		Debug.LogFormat("Server started with socket id: ({0})", hostId);
	}

	public void Update()
	{
		if (!isStarted) return;

		int recHostId;
		int connectionId;
		int channelId;
		byte[] recBuffer = new byte[1024];
		int bufferSize = 1024;
		int dataSize;
		byte error;

		NetworkEventType recData = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, bufferSize, out dataSize, out error);
		switch (recData)
		{
			case NetworkEventType.ConnectEvent:
				Debug.LogFormat("Client: ({0}) has connected", connectionId);
				clientIds.Add(connectionId);
				SendCommand(new OnClientConnectedCommand(connectionId), 0, connectionId);
				break;
			case NetworkEventType.DataEvent:
				ReceiveData(recBuffer, connectionId);
				break;

			case NetworkEventType.DisconnectEvent:
				Debug.LogFormat("Client: ({0}) has disconnected", connectionId);
				clientIds.Remove(connectionId);
				break;
		}
	}

	public void ReceiveData(byte[] data, int connectionId)
	{
		MemoryStream stream = new MemoryStream(data);
		BinaryReader reader = new BinaryReader(stream);

		CommandType cmdType = (CommandType)reader.ReadInt32();

		switch (cmdType)
		{
			case CommandType.Message:
				{
					MessageCommand cmd = new MessageCommand().Deserialize(data) as MessageCommand;
					Debug.LogFormat("Message from client: ({0}) received: {1}", connectionId, cmd.message);
					SendCommand(cmd, 0);
					break;
				}
			case CommandType.Move:
				{
					MoveCommand cmd = new MoveCommand().Deserialize(data) as MoveCommand;
					Debug.LogFormat("Move from client: ({0}) received: {1}", connectionId, cmd.pos);
					SendCommand(cmd, 0);
					break;
				}
		}

		reader.Close();
		stream.Close();
	}

	public void SendData(byte[] data, int channelId, int connectionId)
	{
		byte error;

		List<int> recipients = clientIds;
		if (connectionId != -1)
		{
			recipients = recipients.Where(c => c == connectionId).ToList();
		}

		recipients.ForEach(c =>
		{
			NetworkTransport.Send(hostId, c, channelId, data, data.Length, out error);
			Debug.LogFormat("Data sent from server to client: ({0})", c);
		});
	}

	public void SendCommand(Command command, int channelId, int connectionId = -1)
	{
		SendData(command.Serialize(), channelId, connectionId);
	}

	public int GetId()
	{
		return hostId;
	}
}
