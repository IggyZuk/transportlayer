﻿using UnityEngine;
using UnityEngine.Networking;

public enum CommandType { 
	Move,
	Message,
	OnClientConnected
}

public class SimpleNet : MonoBehaviour
{
	const int MAX_CONNECTION = 4;
	const int PORT = 8888;

	int reliableChannel;
	int unreliableChannel;
	byte error;

	Host host;

	public void StartServer()
	{
		NetworkTransport.Init();
		ConnectionConfig cc = new ConnectionConfig();

		reliableChannel = cc.AddChannel(QosType.Reliable);
		unreliableChannel = cc.AddChannel(QosType.Unreliable);

		HostTopology topology = new HostTopology(cc, MAX_CONNECTION);

		host = new Server(topology, PORT);
	}

	public void StartClient()
	{
		NetworkTransport.Init();
		ConnectionConfig cc = new ConnectionConfig();

		reliableChannel = cc.AddChannel(QosType.Reliable);
		unreliableChannel = cc.AddChannel(QosType.Unreliable);

		HostTopology topology = new HostTopology(cc, MAX_CONNECTION);

		host = new Client(topology, PORT);
	}

	public void SendMessageCommand(string text)
	{
		Debug.Log("SendMessageCommand... " + text);
		host.SendCommand(new MessageCommand(host.GetId(), text), reliableChannel);
	}

	public void SendMoveCommand()
	{
		Debug.Log("SendMoveCommand... " + Input.mousePosition);
		host.SendCommand(new MoveCommand(Input.mousePosition), unreliableChannel);
	}

	void Update()
	{
		if (host != null)
		{
			host.Update();
		}
	}
}



