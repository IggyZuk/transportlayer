﻿using UnityEngine;
using UnityEngine.UI;

public class MenuPanel : MonoBehaviour
{
	[SerializeField]
	Button serverButton;
	[SerializeField]
	Button clientButton;
	[SerializeField]
	InputField inputField;
	[SerializeField]
	Button sendButton;
	[SerializeField]
	Button moveButton;

	[SerializeField]
	SimpleNet simpleNet;

	void Awake()
	{
		serverButton.onClick.AddListener(() =>
		{
			simpleNet.StartServer();
		});

		clientButton.onClick.AddListener(() =>
		{
			simpleNet.StartClient();
		});

		sendButton.onClick.AddListener(() =>
		{
			simpleNet.SendMessageCommand(inputField.text);
		});

		moveButton.onClick.AddListener(() =>
		{
			simpleNet.SendMoveCommand();
		});
	}
}
